%include "colon.inc"
%include "lib.inc"

section .data
%include "words.inc"

section .rodata
logn_key_warning: db "Key is too long!", 0x0a, 0
key_not_found_warning: db "Key was not found.", 0x0a, 0
val_str: db "Value: ", 0

section .text

global _start

_start:
    sub rsp, 100h
    mov rdi, rsp
    mov rsi, 100h
    call read_word

    test rax, rax
    je .too_long_error

    mov rdi, rax
    mov rsi, dict
    call find_word

    test rax, rax
    je .not_found_error

    mov rdi, val_str
    call print_string

    mov rdi, [rax + 8]
    call print_string
    call print_newline

    xor rdi, rdi
    call exit

.too_long_error:
    mov rdi, logn_key_warning
    call print_string
    call print_newline

    mov rdi, 1
    call exit


.not_found_error:
    mov rdi, key_not_found_warning
    call print_string
    call print_newline

    mov rdi, 2
    call exit
