global find_word
extern string_equals

section .text

; rdi - key to find
find_word:
    .loop:
        push rdi
        push rsi
        add rsi, 10h
        call string_equals
        pop rsi
        pop rdi

        test rax, 1
        jnz .g_end

        mov rsi, [rsi]
        test rsi, rsi
        jnz .loop

        xor rax, rax
        ret

    .g_end:
        mov rax, rsi
        ret
